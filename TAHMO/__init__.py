# Module dependancies
import dateutil.parser
import datetime
import gc

import requests
import pandas as pd

# Module Constants
API_BASE_URL = 'https://datahub.tahmo.org'
API_MAX_PERIOD = '365D'

class apiWrapper(object):
    apiKey = ''
    apiSecret = ''

    def setCredentials(self, key, secret):
        self.apiKey = key
        self.apiSecret = secret

    def getMeasurements(self, station, startDate=None, endDate=None, variables=None, dataset='controlled'):
        endpoint = 'services/measurements/v2/stations/%s/measurements/%s' % (station, dataset)

        dateSplit = self.__splitDateRange(startDate, endDate)
        measurements = []

        for index, row in dateSplit.iterrows():
            params = {'start': row['start'].strftime('%Y-%m-%dT%H:%M:%SZ'), 'end': row['end'].strftime('%Y-%m-%dT%H:%M:%SZ')}
            if variables and isinstance(variables, list) and len(variables) == 1:
                params['variable'] = variables[0]
            response = self.__request(endpoint, params)
            if 'results' in response and len(response['results']) >= 1 and 'series' in response['results'][0] and len(
                response['results'][0]['series']) >= 1 and 'values' in response['results'][0]['series'][0]:

                for result in response['results']:
                    if 'series' in result and len(result['series']) >= 1 and 'values' in result['series'][0]:
                        for serie in result['series']:

                            columns = serie['columns']
                            observations = serie['values']

                            time_index = columns.index('time')
                            quality_index = columns.index('quality')
                            variable_index = columns.index('variable')
                            sensor_index = columns.index('sensor')
                            station_index = columns.index('station')
                            value_index = columns.index('value')

                            for element in observations:
                                measurements.append([element[time_index], element[station_index], element[sensor_index], element[variable_index], element[quality_index], element[value_index]])

                            # Clean up scope.
                            del columns
                            del observations

                # Clean up scope and free memory.
                del response
                gc.collect()

        return measurements

    def getStations(self):
        response = self.__request('services/assets/v2/stations', {'sort': 'code'})
        stations = {}
        if 'data' in response and isinstance(response['data'], list):
            for element in response['data']:
                stations[element['code']] = element
        return stations

    def getStation(self, stationId):
        response = self.__request('services/assets/v2/stations/%s' % stationId, {})
        if 'data' in response and 'station' in response['data']:
            return response['data']['station']
        else:
            raise ValueError("Invalid station request")

    def getSensor(self, sensorId):
        response = self.__request('services/assets/v2/sensors/%s' % sensorId, {})
        if 'data' in response and 'sensor' in response['data']:
            return response['data']['sensor']
        else:
            raise ValueError("Invalid sensor request")

    def __request(self, endpoint, params):
        print('API request start: %s' % endpoint)
        apiRequest = requests.get(
            '%s/%s' % (API_BASE_URL, endpoint),
            params=params,
            auth=requests.auth.HTTPBasicAuth(
                self.apiKey,
                self.apiSecret
            ),
            timeout=60
        )
        print('API request finished: %s' % endpoint)

        if apiRequest.status_code == 200:
            return apiRequest.json()
        else:
            return self.__handleApiError(apiRequest)

    # Split date range into intervals of 365 days.
    def __splitDateRange(self, inputStartDate, inputEndDate):
        try:
            startDate = dateutil.parser.parse(inputStartDate)
            endDate = dateutil.parser.parse(inputEndDate)
        except ValueError:
            raise ValueError("Invalid data parameters")

        # Split date range into intervals of 365 days.
        dates = pd.date_range(start=startDate.strftime("%Y%m%d"), end=endDate.strftime("%Y%m%d"), freq=API_MAX_PERIOD)

        df = pd.DataFrame([[i, x] for i, x in
                           zip(dates, dates.shift(1) - datetime.timedelta(seconds=1))],
                          columns=['start', 'end'])

        # Set start and end date to their provided values.
        df.loc[0, 'start'] = pd.Timestamp(startDate)
        df['end'].iloc[-1] = pd.Timestamp(endDate)
        return df

    def __handleApiError(self, apiRequest):
        json = None
        try:
            # Try to parse json and check if body contains a specific error message.
            json = apiRequest.json()
        finally:
            if json and 'error' in json and 'message' in json['error']:
                raise Exception(json['error']['message'])
            else:
                raise Exception('API request failed with status code %s' % apiRequest.status_code)
