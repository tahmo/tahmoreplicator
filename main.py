# Import dependencies.
import TAHMO
import schedule
import datetime
import dateutil
import os
from models import Base, Measurement, Station, Sensor
from sqlalchemy import create_engine, desc
from sqlalchemy.orm import sessionmaker

# Load environment variables from file.
from dotenv import load_dotenv
load_dotenv()

# Initialize database session.
db_uri = os.getenv('SQLALCHEMY_DATABASE_URI')
engine = create_engine(os.getenv('DB_URI'))
Session = sessionmaker(bind=engine)

# Create all database tables.
Base.metadata.create_all(engine)

# Initialize TAHMO API.
api = TAHMO.apiWrapper()
api.setCredentials(os.getenv('TAHMO_API_USER'), os.getenv('TAHMO_API_PASSWORD'))


def updateMetadata():
    print('Update stations')
    session = Session()
    try:
        # Request station information from API.
        apiStations = api.getStations()

        # Create station objects from retrieved data.
        for stationCode in apiStations.keys():
            station = api.getStation(apiStations[stationCode]['id'])
            session.merge(Station(
                code=station['code'],
                name=station['location']['name'],
                locationid=station['location']['id'],
                countrycode=station['location']['countrycode'],
                latitude=station['location']['latitude'],
                longitude=station['location']['longitude'],
                elevationmsl=station['location']['elevationmsl'],
            ))

            # Check if there is new sensor metadata.
            for sensorinstallation in station['sensorinstallations']:
                sensorCode = 'S%s' % str(sensorinstallation['sensorinstallation']['sensorid']).zfill(6)
                if (session.query(Sensor).filter(Sensor.code == sensorCode).first() == None):
                    sensor = api.getSensor(sensorinstallation['sensorinstallation']['sensorid'])
                    session.add(Sensor(
                        code=sensorCode,
                        type=sensor['sensortype']['name'],
                        elevationground=sensorinstallation['sensorinstallation']['elevationground'],
                    ))

        session.commit()
    except Exception as e:
        print('Error while retrieving stations: %s' % str(e))
    finally:
        session.close()


def updateMeasurements():
    print('Update measurements')
    session = Session()
    stations = session.query(Station).all()
    session.close()
    for station in stations:
        updateMeasurementsForStation(station.code)


def updateMeasurementsForStation(stationCode):
    print('Update measurements for station %s' % stationCode)
    session = Session()
    try:
        lastMeasurement = session.query(Measurement).filter(Measurement.station == stationCode).order_by(desc(Measurement.time)).first()
        startDate = ((lastMeasurement.time + datetime.timedelta(seconds=1)) if lastMeasurement else dateutil.parser.parse(os.getenv('START_TIME')))

        monthPrefix = startDate.strftime('%Y%m')
        monthStartDate = dateutil.parser.parse("%s01T00:00:00Z" % monthPrefix)
        monthEndDate = monthStartDate + dateutil.relativedelta.relativedelta(months=+1) - datetime.timedelta(seconds=1)

        measurementElements = api.getMeasurements(stationCode, startDate=startDate.strftime('%Y%m%d%H%M%S'), endDate=monthEndDate.strftime('%Y%m%d%H%M%S'))

        if len(measurementElements):
            print('Saving %i measurements to db' % len(measurementElements))
            measurementObjects = map(
                lambda m: Measurement(time=datetime.datetime.strptime(m[0], "%Y-%m-%dT%H:%M:%SZ"), station=m[1], sensor=m[2], variable=m[3],
                                      quality=m[4], value=m[5]), measurementElements)
            session.add_all(measurementObjects)
            session.commit()

    except Exception as e:
        print('Error while retrieving measurements for station %s: %s' % (stationCode, str(e)))
    finally:
        print('Finished updating measurements for station %s' % stationCode)
        session.close()


print('Scheduling jobs')
schedule.every(4).hour.at(":00").do(updateMetadata)
schedule.every(1).hour.at(":20").do(updateMeasurements)

while True:
    schedule.run_pending()
