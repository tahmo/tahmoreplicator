from sqlalchemy import Column, Integer, String, DateTime, Float, ForeignKey, PrimaryKeyConstraint
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Measurement(Base):
    """Measurement data"""
    __tablename__ = "measurement"
    __table_args__ = (
        PrimaryKeyConstraint('time', 'station', 'sensor', 'variable'),
        {}
    )

    time = Column(DateTime, nullable=False)
    station = Column(String(7), ForeignKey('station.code'), nullable=False)
    # Note: Foreign key is not used for sensor code because sensors are synchronized asynchronous from data.
    sensor = Column(String(7), nullable=False)
    variable = Column(String(3),nullable=False)
    quality = Column(Integer, nullable=False)
    value = Column(Float, nullable=True)

class Station(Base):
    """Station information"""
    __tablename__ = "station"

    code = Column(String(7), primary_key=True, nullable=False)
    name = Column(String(100), nullable=False)
    locationid = Column(Integer, nullable=False)
    countrycode = Column(String(2), nullable=False)
    latitude = Column(Float, nullable=False)
    longitude = Column(Float, nullable=False)
    elevationmsl = Column(Float, nullable=False)


class Sensor(Base):
    """Sensor information"""
    __tablename__ = "sensor"

    code = Column(String(7), primary_key=True, nullable=False)
    type = Column(String(100), nullable=False)
    elevationground = Column(Float, nullable=False)